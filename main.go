package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func main() {
	cmd := make([]string, len(os.Args))
	for index, value := range os.Args[1:] {
		cmd[index] = value
	}

	// cmd = ptt3.exe macshop 3
	// 第一個參數 macshop 是要抓取的看板名稱
	// 3 是要抓取的頁數
	numbers, err := strconv.Atoi(cmd[1])
	if err != nil {
		fmt.Println("Not Number")
		fmt.Println(err)
	}

	url := "https://www.ptt.cc/bbs/" + cmd[0] + "/index.html"

	title, PNhref := getData2(url, numbers)

	show2(title)

	fmt.Print("Input : ")
	reader := bufio.NewReader(os.Stdin)
	for {
		text, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			break
		}

		inputInstruction := strings.Split(text, " ")

		for i := range inputInstruction {
			inputInstruction[i] = strings.Replace(inputInstruction[i], "\n", "", -1)
			inputInstruction[i] = strings.Replace(inputInstruction[i], "\r", "", -1)
		}
		// text = strings.Replace(text, "\n", "", -1)
		// text = strings.Replace(text, "\r", "", -1)

		inputLen := len(inputInstruction)
		switch inputInstruction[0] {
		case "c":
			{
				fmt.Println(text + "I")
				cleanConsole()
				break
			}
		case "s":
			{
				//show(title, href)
				break
			}
		case "exit":
			{
				return
			}
		case "u":
			{
				// var pages int
				// if inputLen == 1 {
				// 	pages = numbers
				// } else {
				// 	tpages, err := strconv.Atoi(inputInstruction[1])
				// 	if err != nil {
				// 		fmt.Println("Not Number")
				// 		fmt.Println(err)
				// 	}
				// 	pages = tpages
				// }
				// title, href ;= getData(cmd[0], pages)
				// cleanConsole()
				// show(title, href)
				break
			}
		case "o":
			{
				if inputLen == 1 {
					fmt.Println("Please input which index you want to open!")
					continue
				}
				hrefindex, err := strconv.Atoi(inputInstruction[1])
				if err != nil {
					fmt.Println("Not Number")
					fmt.Println(err)
				}
				tempcmd := exec.Command("cmd", "/c", "Chrome.lnk -incognito -app="+title[hrefindex][2])
				tempcmd.Stdout = os.Stdout
				tempcmd.Run()
			}
		case "d":
			{
				hrefindex, err := strconv.Atoi(inputInstruction[1])
				if err != nil {
					fmt.Println("Not Number")
					fmt.Println(err)
				}
				//fmt.Printf("%s", href[hrefindex])
				DownloadPic(title[hrefindex][2])
				break
			}
		case "p":
			{
				if PNhref[0] == "" {
					fmt.Println("No Next Page")
				} else {
					title, PNhref = getData2(PNhref[0], 1)
					cleanConsole()
					show2(title)
				}
				break
			}
		case "n":
			{
				if PNhref[1] == "" {
					fmt.Println("No Next Page")
				} else {
					title, PNhref = getData2(PNhref[1], 1)
					cleanConsole()
					show2(title)
				}
				break
			}
		default:
			{
				fmt.Println(text + "id")
				// tempcmd := exec.Command("cmd", "/c", "cls")
				// tempcmd.Stdout = os.Stdout
				// tempcmd.Run()
			}
		}
	}
}

func show(title []string, href []string) {
	for i := 0; i < len(title); i++ {
		fmt.Println(strconv.Itoa(i) + ".  " + title[i])
	}
}

func show2(title [][]string) {
	for i := 0; i < len(title); i++ {
		fmt.Println(strconv.Itoa(i) + ".  " + title[i][0] + "  " + title[i][1])
	}
}

func cleanConsole() {
	tempcmd := exec.Command("cmd", "/c", "cls")
	tempcmd.Stdout = os.Stdout
	tempcmd.Run()
}

//Chrome.lnk -incognito -app=http://www.google.com.tw
