package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
)

var wg sync.WaitGroup

// DownloadPic => Download picture
func DownloadPic(url string) {
	//picURL := "https://i.imgur.com/xXFZkah.jpg"
	//url := "https://www.ptt.cc/bbs/TypeMoon/M.1533834254.A.8CD.html"

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println("Fatal error", err.Error())
		os.Exit(0)
	}

	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		fmt.Println("Fatal error ", err.Error())
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	//fmt.Println(doc.Text())

	dirName := getDirName(url)

	doc.Find("#main-content").Each(func(i int, s *goquery.Selection) {
		//href, exist := s.Find("a").Attr("href")

		if _, err := os.Stat(dirName); os.IsNotExist(err) {
			err := os.Mkdir(dirName, 0700)
			if err != nil {
				fmt.Println(err)
			}
		}

		err = os.Chdir(dirName)
		if err != nil {
			fmt.Println(err)
		}

		s.Find("a").Each(func(j int, s2 *goquery.Selection) {
			href, exist := s2.Attr("href")
			if exist {
				if strings.Contains(href, ".jpg") || strings.Contains(href, ".png") {
					wg.Add(1)
					go downIt(j, href)
				}
			}

		})
	})

	wg.Wait()
	os.Chdir("../") // 返回上一層
	fmt.Println("Download Finish")

}

func downIt(j int, href string) {
	respPic, _ := http.Get(href)
	body, _ := ioutil.ReadAll(respPic.Body)
	out, _ := os.Create(strconv.Itoa(j) + ".jpg")
	io.Copy(out, bytes.NewReader(body))

	wg.Done()
}

// DownloadAll => Download whole page
func DownloadAll(url string) {

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println("Fatal error", err.Error())
		os.Exit(0)
	}

	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		fmt.Println("Fatal error ", err.Error())
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Fatal error ", err.Error())
		return
	}

	// if exist file => delete
	fileName := getDirName(url)

	err = ioutil.WriteFile(fileName+".html", body, 0700)
	if err != nil {
		fmt.Println("Fatal error ", err.Error())
	}
	//fmt.Println(doc.Text())
}

func getDirName(url string) string {
	tempStrArr1 := strings.Split(url, "/")
	tempStr1 := tempStrArr1[len(tempStrArr1)-1]

	tempStrArr2 := strings.Split(tempStr1, ".")
	return tempStrArr2[1]
}
