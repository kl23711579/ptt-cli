package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

func transTime(str string) string {
	temp := strings.Split(strings.Split(str, "/")[3], ".")
	unixtime, err := strconv.ParseInt(temp[1], 10, 64)
	if err != nil {
		fmt.Println("Time Error")
		fmt.Println(err)
	}

	tm := time.Unix(unixtime, 0)
	return tm.Format("2006-01-02 15:04:05")

}
