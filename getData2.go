package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func getData2(url string, pages int) ([][]string, [2]string) {
	cookie := http.Cookie{Name: "over18", Value: "1"}
	titleResult := make([][]string, 0)

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println("Fatal error ", err.Error())
		os.Exit(0)
	}

	request.AddCookie(&cookie)

	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		fmt.Println("Fatal error ", err.Error())
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find(".r-ent").Each(func(i int, s *goquery.Selection) {
		titleResult = append(titleResult, make([]string, 0))

		// popularitu
		nrec := s.Find(".nrec").Children()
		if nrec.Length() == 0 {
			titleResult[i] = append(titleResult[i], "0")
		} else {
			titleResult[i] = append(titleResult[i], nrec.First().Text())
		}

		//time and title
		titleNode := s.Find(".title")
		title := titleNode.Find("a").Text()
		href, exist := titleNode.Find("a").Attr("href")
		if exist {
			time := transTime(href)

			titleResult[i] = append(titleResult[i], time+"  =>  "+title)
			titleResult[i] = append(titleResult[i], "https://www.ptt.cc/"+href)
		} else {
			text := strings.TrimSpace(titleNode.Text())
			titleResult[i] = append(titleResult[i], text)
			titleResult[i] = append(titleResult[i], "")
		}
	})

	// doc.Find(".btn-group-paging a").Each(func(i int, s *goquery.Selection) {
	// 	// if strings.Contains(s.Text(), "上頁")
	// 	if i == 1 {
	// 		url, _ = s.Attr("href")
	// 	}
	// 	fmt.Println(url)
	// })

	pageButton := doc.Find(".btn-group-paging a") // 這時候指向第一個 => 最舊
	urlP, exist := pageButton.Next().Attr("href") // Next() 讓指標指向下一個，也就是上頁

	if exist {
		urlP = "https://www.ptt.cc/" + urlP // 上一頁的網址
	} else {
		urlP = ""
	}

	urlN, exist := pageButton.Next().Next().Attr("href")

	if exist {
		urlN = "https://www.ptt.cc/" + urlN // 上一頁的網址
	} else {
		urlN = ""
	}

	PNResult := [2]string{urlP, urlN}
	fmt.Println(PNResult)
	//fmt.Println(url)

	return titleResult, PNResult
}
