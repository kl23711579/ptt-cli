package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/PuerkitoBio/goquery"
)

func getData(board string, pages int) ([]string, []string) {
	url := "https://www.ptt.cc/bbs/" + board + "/index.html"
	cookie := http.Cookie{Name: "over18", Value: "1"}
	titleResult := make([]string, 0)
	hrefResult := make([]string, 0)

	for i := 0; i < pages; i++ {
		temptitleResult := titleResult
		temphrefResult := hrefResult

		titleResult = []string{}
		hrefResult = []string{}

		request, err := http.NewRequest("GET", url, nil)
		if err != nil {
			fmt.Println("Fatal error ", err.Error())
			os.Exit(0)
		}
		request.AddCookie(&cookie)

		client := &http.Client{}
		resp, err := client.Do(request)
		if err != nil {
			fmt.Println("Fatal error ", err.Error())
		}

		doc, err := goquery.NewDocumentFromReader(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		doc.Find(".title").Each(func(i int, s *goquery.Selection) {
			title := s.Find("a").Text()
			href, exist := s.Find("a").Attr("href")
			if exist {
				// fmt.Println("https://www.ptt.cc/" + href + "   =>   " + title)
				time := transTime(href)
				titleResult = append(titleResult, time+"  =>  "+title)

				hrefResult = append(hrefResult, "https://www.ptt.cc/"+href)
			}
		})

		// doc.Find(".btn-group-paging a").Each(func(i int, s *goquery.Selection) {
		// 	// if strings.Contains(s.Text(), "上頁")
		// 	if i == 1 {
		// 		url, _ = s.Attr("href")
		// 	}
		// 	fmt.Println(url)
		// })

		pageButton := doc.Find(".btn-group-paging a") // 這時候指向第一個 => 最舊
		url, _ = pageButton.Next().Attr("href")       // Next() 讓指標指向下一個，也就是上頁

		url = "https://www.ptt.cc/" + url // 上一頁的網址
		//fmt.Println(url)

		// 合併原本和temp
		titleResult = append(titleResult, temptitleResult...)
		hrefResult = append(hrefResult, temphrefResult...)

	}

	return titleResult, hrefResult
}
